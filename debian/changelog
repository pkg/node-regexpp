node-regexpp (3.2.0-4+apertis2) apertis; urgency=medium

  * Add missing link to undici-types
      Fix build on nodejs 18.13 and 18.19 update
      https://salsa.debian.org/js-team/node-regexpp/-/commit/7f74a551

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Fri, 16 Feb 2024 15:14:18 +0100

node-regexpp (3.2.0-4+apertis1) apertis; urgency=medium

  * Sync updates from debian/bookworm
  * Disable pandoc build dependency to avoid pulling in the whole cluster
    of package dependencies.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Apr 2023 22:47:57 +0000

node-regexpp (3.2.0-4) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Apply multi-arch hints. + node-regexpp: Add Multi-Arch: foreign.

  [ Yadd ]
  * Update standards version to 4.6.1, no changes needed.
  * Add fix for rollup 3 (Closes: #1022649)
  * Fix nocheck target

 -- Yadd <yadd@debian.org>  Thu, 27 Oct 2022 13:54:52 +0200

node-regexpp (3.2.0-3) unstable; urgency=medium

  * generate documentation with cmark-gfm (not pandoc);
    build-depend on cmark-gfm (not pandoc)
  * tighten lintian overrides

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 28 Feb 2022 11:28:19 +0100

node-regexpp (3.2.0-2) unstable; urgency=medium

  * Team Upload.
  * d/tests/control: Check require in the AUTOPKGTEST_TMP
    instead of source dir (Closes: #996715)

 -- Nilesh Patra <nilesh@debian.org>  Sun, 17 Oct 2021 23:51:41 +0530

node-regexpp (3.2.0-1) unstable; urgency=medium

  [ upstream ]
  * new release

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 23 Sep 2021 17:50:25 +0200

node-regexpp (3.1.0-7) unstable; urgency=medium

  * check testsuite;
    build-depend on mocha node-types-eslint ts-node

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 23 Sep 2021 17:38:47 +0200

node-regexpp (3.1.0-6) unstable; urgency=medium

  * update copyright info:
    + use Reference field (not License-Reference);
      tighten lintian overrides
    + update coverage
  * simplify source helper script copyright-check
  * declare compliance with Debian Policy 4.6.0
  * use debhelper compatibility level 13 (not 12)
  * update git-buildpackage config: add usage comment
  * unfuzz patch 2002

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 23 Sep 2021 16:45:14 +0200

node-regexpp (3.1.0-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository, Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 29 Aug 2021 01:11:07 +0100

node-regexpp (3.1.0-4apertis1) apertis; urgency=medium

  * debian: Disable pandoc documentation. Avoid pulling in the whole cluster
    of package dependencies.

 -- Ariel D'Alessandro <ariel.dalessandro@collabora.com>  Mon, 03 May 2021 09:24:29 -0300

node-regexpp (3.1.0-4apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Wed, 21 Apr 2021 11:21:56 +0200

node-regexpp (3.1.0-4) unstable; urgency=medium

  * add patch 1001 to use newer namespaced rollup plugin
  * tighten to build-depend versioned
    on node-rollup-plugin-node-resolve

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 01 Jan 2021 06:19:51 +0100

node-regexpp (3.1.0-3) unstable; urgency=medium

  * fix have autopkgtest depend on nodejs

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 27 Dec 2020 04:34:16 +0100

node-regexpp (3.1.0-2) unstable; urgency=medium

  * fix long description
    (had bogus copy-paste leftover from unrelated package)
  * copyright: shorten Source URL
  * unconditionally skip refresh unicode ids and properties
    (not only when targeted experimental)

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 13 Dec 2020 23:50:04 +0100

node-regexpp (3.1.0-1) experimental; urgency=low

  * initial release;
    closes: bug#945999

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 06 Dec 2020 11:55:56 +0100
